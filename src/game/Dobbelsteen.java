package game;

/**
 * De klasse <code>Dobbelsteen</code> representeert een dobbelsteen.
 * 
 * @author Maikel Eckelboom
 * @version 0.1
 * @see Paneel
 * 
 */
import java.awt.*;

import javax.swing.JComponent;


public class Dobbelsteen extends JComponent {
		
	// Serial Version UID
	private static final long serialVersionUID = 1L;
	
	// Instantie Variabelen
	public int diameter, werpen, ijsberen, pinguins, wakken;	
	
	// Constructor voor de klasse Dobbelsteen.
	public Dobbelsteen() {
		diameter = 12;
		this.setPreferredSize(new Dimension(129, 129));		
	}
	
	// Bereken andere kant van de dobbelsteen
	public int andereKant() {
		return (6 - this.werpen) + 1; 
	}
		
	public int WerpDobbelsteen() {
		// Werpen integer
		werpen = (int) (6 * Math.random() + 1);
		
		// Print int werpen naar console
		System.out.print("Geworpen: " + werpen);
		
		// Revalidate
		this.revalidate();
		
		// Repaint
		this.repaint();
				
		// Return
		return werpen;
	}

    
	public void paintComponent(Graphics g) {
		
		super.paintComponent(g);

		Graphics2D g2d = (Graphics2D) g;
		
		// Set anti-alias
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	            			 RenderingHints.VALUE_ANTIALIAS_ON); 

		// Set anti-alias for text
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
	            			 RenderingHints.VALUE_TEXT_ANTIALIAS_ON); 
		
		// Variables
		int diceRadius = 13,	diceWidth = 100,	diceHeight = 100;
		int dotLeftX = 78, 		dotLeftY = 48;
		int dotMiddle = 52, 	dotMiddleY = 72;
		int dotRightX = 28, 	dotRightY = 98;
		int offsetX = 10, 		offsetY = 28; 

		// Bereken het middelpunt
		int pos = (100 / 2) - (diameter / 2); 

		
		// Default waardes dobbelsteen
		g.setColor(Color.WHITE);
		g.fillRoundRect(offsetX, 	offsetY, 
						diceWidth, 	diceHeight, 
						diceRadius, diceRadius);
		g.setColor(Color.BLACK);
		g.drawRoundRect(offsetX, 	offsetY, 
						diceWidth, 	diceHeight, 
						diceRadius, diceRadius);
		

		// Switch case werpen
		switch (werpen) {
			case 1:
				g.fillOval(pos + offsetX, pos + offsetY, diameter, diameter);
				wakken = 1;
				pinguins = 6;
				break;	
			case 2:
				g.fillOval(dotLeftX , dotLeftY, diameter, diameter);
				g.fillOval(dotRightX, dotRightY, diameter, diameter);
				pinguins = 5;
				break;	
			case 3:
				g.fillOval(dotLeftX, dotLeftY, diameter, diameter);
				g.fillOval(dotMiddle, dotMiddleY, diameter, diameter);
				g.fillOval(dotRightX, dotRightY , diameter, diameter);
				wakken = 1;
				ijsberen = 2;
				pinguins = 4;
				break;
			case 4:
				g.fillOval(dotLeftX, dotLeftY, diameter, diameter);
				g.fillOval(dotLeftX, dotRightY, diameter, diameter);
				g.fillOval(dotRightX, dotLeftY, diameter, diameter);
				g.fillOval(dotRightX, dotRightY , diameter, diameter);
				pinguins = 3;
				break;		
			case 5:
				g.fillOval(dotLeftX, dotLeftY, diameter, diameter);
				g.fillOval(dotLeftX, dotRightY, diameter, diameter);
				g.fillOval(dotMiddle, dotMiddleY, diameter, diameter);
				g.fillOval(dotRightX, dotLeftY, diameter, diameter);
				g.fillOval(dotRightX, dotRightY, diameter, diameter);
				wakken = 1;
				ijsberen = 4;
				pinguins = 2;
				break;	
			case 6:
				g.fillOval(dotLeftX, dotLeftY, diameter, diameter);
				g.fillOval(dotLeftX, dotMiddleY, diameter, diameter);
				g.fillOval(dotLeftX, dotRightY, diameter, diameter);
				g.fillOval(dotRightX, dotLeftY, diameter, diameter);
				g.fillOval(dotRightX, dotMiddleY, diameter, diameter);
				g.fillOval(dotRightX, dotRightY, diameter, diameter);
				pinguins = 1;
				break;	

		}
	
	}
	
}