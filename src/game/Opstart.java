/**
* @author Maikel Eckelboom
* @param 97008912
* @since 13-01-2020
* @version 1.0
*/

package game;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;


public class Opstart extends JFrame {
	
	private static final long serialVersionUID = 1L;


	public static void main (String[] args) {
		
		/**
		 * 1. Roep klasse JFrame aan
		 * 2. Gebruik dit als basis voor object frame
		 * 3. Geef aan dat het om een nieuw object gaat
		 * 4. koppel het aan de klasse Opstart()	 
 		*/
		
		JFrame frame = new Opstart();
		frame.setSize(1050, 900);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocation(500, 100);
		frame.setTitle("Het ijsberen spel");
		frame.setVisible(true);
     
		
		// JPanel object
		JPanel Panel = new Paneel();
		frame.setContentPane(Panel);
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Anti-aliasing voor text
		System.setProperty("awt.useSystemAAFontSettings","on");
		System.setProperty("swing.aatext", "true");

		
		// Frame Favicon
		ImageIcon img = new ImageIcon("C:\\wakkenFiles/penguin_5.png");
		frame.setIconImage(img.getImage());
	
		frame.addWindowListener( new WindowAdapter() {
		    public void windowOpened( WindowEvent e ){
		        spinner.requestFocus();
		    }
		}); 
		
	
	}
	
}