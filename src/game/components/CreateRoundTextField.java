package game.components;

import java.awt.Color;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JTextField;

public class CreateRoundTextField extends JTextField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Shape shape;
	
	public int withZeroX;

	public CreateRoundTextField() {
		setOpaque(false);
	}
	

	protected void paintComponent(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;

		// Set anti-alias
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// Set anti-alias for text
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
		g2d.setColor(getBackground());
		
		g2d.fillRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 12, 12);

		super.paintComponent(g);
	}

	public void paintBorder(Graphics g) {
//		Graphics2D g2d = (Graphics2D) g;

		// Set anti-alias
//		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		// Set anti-alias for text
//		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		
//		g2d.setColor(new Color(0XF1F1F1));
		
//		g2d.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, 12, 12);
	}

	public boolean contains(int x, int y) {
		if (shape == null || !shape.getBounds().equals(getBounds())) {
			shape = new RoundRectangle2D.Float(0, 0, getWidth() - 1, getHeight() - 1, 12, 12);
		}
		return shape.contains(x, y);
	}
}