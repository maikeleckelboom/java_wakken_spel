package game.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.Border;

	public class CreateRoundButton implements Border {

	    private int radius;


	    public CreateRoundButton(int radius) {
	        this.radius = radius;
	    }


	    public Insets getBorderInsets(Component c) {
	        return new Insets(this.radius+1, this.radius+1, this.radius+2, this.radius);
	    }


	    public boolean isBorderOpaque() {
	        return true;
	    }


	    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {

			Graphics2D g2d = (Graphics2D) g;
			
			// Set anti-alias
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		            			 RenderingHints.VALUE_ANTIALIAS_ON); 

			// Set anti-alias for text
			g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
		            			 RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
	    	g2d.drawRoundRect(x, y, width-1, height-1, radius, radius);
	        
	    }
	}
