package game;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultFormatter;
import game.components.CreateRoundTextField;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Paneel extends JPanel implements DocumentListener {

	/**
	 * Generated VersionUID
	 */

	private static final long serialVersionUID = 1L;

	/**
	 * Declaraties
	 */

	// Knoppen
	private JButton nieuwSpelButton, werpButton, controlerenButton, ophogenButton, verlagenButton, oplossingButton;

	// Labels
	private JLabel totaalGeworpenLabel, aantalGoedLabel, aantalFoutLabel, hintsLabel, juisteAntwoordenLabel,
			aantalGeworpenCounter, aantalGeworpenLabel, jouwAntwoordenLabel, aantalDobbelstenenLabel, wakkenLabel,
			pinguinsLabel, ijsberenLabel, titelLabel, printPinguinsFieldValueLabel, printIjsberenFieldValueLabel,
			printWakkenFieldValueLabel, printGoedGeradenFieldValueLabel, printFoutGeradenFieldValueLabel,
			aantalDobbelsLabel, pinguin_1, pinguin_2, ijsbeer_1, pinguin_4, pinguin_5;

	// Tekstvelden
	private JTextField aantalIjsberenField, aantalPinguinsField, aantalWakkenField, aantalFoutField, aantalGoedField,
			aantalgeworpenField, aantalDobbelstenenField, pinguinsFieldValueAnswer, ijsberenFieldValueAnswer,
			wakkenFieldValueAnswer, pinguinsFieldAnswer, ijsberenFieldAnswer, wakkenFieldAnswer;

	// Afbeelding iconen
	private ImageIcon imageIcon_1, imageIcon_2, imageIcon_4, imageIcon_5, imageIcon_6;

	// JPanelen
	private JPanel headerPaneel, dobbelstenenPaneel, bedieningsPaneel, controlerenPaneel, gridPaneel, hintsPaneel,
			oplossingPaneel;

	// JSpinner
	private JSpinner spinner;

	// Strings
	private String stringHuidigAantal, huidigAantalDobbelstenen;

	// Integers
	public int totaalGeworpen, checkWakken, checkIjsberen, checkPinguins, wakkenInput, aantalGoedFieldValue,
			aantalFoutFieldValue;

	// Primary Font
	public Font font = new Font("Roboto", Font.BOLD, 14);

	// ArrayList met alle tekstvelden
	private ArrayList<JTextField> fieldList = new ArrayList<JTextField>();
	{
		// Tekstvelden toevoegen aan arrayList
		fieldList.add(aantalFoutField = new CreateRoundTextField());
		fieldList.add(aantalGoedField = new CreateRoundTextField());
		fieldList.add(aantalgeworpenField = new CreateRoundTextField());
		fieldList.add(aantalDobbelstenenField = new CreateRoundTextField());
		fieldList.add(aantalIjsberenField = new CreateRoundTextField());
		fieldList.add(aantalPinguinsField = new CreateRoundTextField());
		fieldList.add(aantalWakkenField = new CreateRoundTextField());
		fieldList.add(pinguinsFieldValueAnswer = new CreateRoundTextField());
		fieldList.add(ijsberenFieldValueAnswer = new CreateRoundTextField());
		fieldList.add(wakkenFieldValueAnswer = new CreateRoundTextField());
		fieldList.add(pinguinsFieldAnswer = new CreateRoundTextField());
		fieldList.add(ijsberenFieldAnswer = new CreateRoundTextField());
		fieldList.add(wakkenFieldAnswer = new CreateRoundTextField());

	}

	// Required fields list | Enable buttos
	private List<JTextField> reqFieldList = null;

	// ArrayList met alle buttons
	private ArrayList<JButton> buttonList = new ArrayList<JButton>();
	{
		// Knoppen toevoegen aan arrayList
		buttonList.add(ophogenButton = new RoundedCornerButton("", new ImageIcon("resources/plus-16.png")));
		buttonList.add(verlagenButton = new RoundedCornerButton("", new ImageIcon("resources/minus-16.png")));
		buttonList.add(controlerenButton = new RoundedCornerButton("Controleren"));
		buttonList.add(nieuwSpelButton = new RoundedCornerButton("Nieuw spel"));
		buttonList.add(werpButton = new RoundedCornerButton("Werpen"));
		buttonList.add(oplossingButton = new RoundedCornerButton("De oplossing"));
	}

	// ArrayList met alle labels
	private ArrayList<JLabel> labelList = new ArrayList<JLabel>();
	{
		labelList.add(totaalGeworpenLabel = new JLabel("null"));
		labelList.add(aantalGeworpenCounter = new JLabel("null"));
		labelList.add(titelLabel = new JLabel("Wakken, ijsberen en pinguins"));
		labelList.add(wakkenLabel = new JLabel("Wakken"));
		labelList.add(pinguinsLabel = new JLabel("Pingu�ns"));
		labelList.add(ijsberenLabel = new JLabel("IJsberen"));
		labelList.add(printWakkenFieldValueLabel = new JLabel("Aantal Wakken:"));
		labelList.add(printPinguinsFieldValueLabel = new JLabel("Aantal Pinguins:"));
		labelList.add(printIjsberenFieldValueLabel = new JLabel("Aantal Ijsberen:"));
		labelList.add(aantalDobbelstenenLabel = new JLabel("Aantal dobbelstenen (3 - 12)"));
		labelList.add(aantalGeworpenLabel = new JLabel("Totaal aantal keer geworpen:"));
		labelList.add(aantalGoedLabel = new JLabel("Aantal keer goed geraden:"));
		labelList.add(aantalFoutLabel = new JLabel("Aantal keer fout geraden:"));
		labelList.add(jouwAntwoordenLabel = new JLabel("Jouw Antwoord"));
		labelList.add(juisteAntwoordenLabel = new JLabel("Juiste Antwoord"));
		labelList.add(hintsLabel = new JLabel("Hints"));
		labelList.add(printGoedGeradenFieldValueLabel = new JLabel("null"));
		labelList.add(printFoutGeradenFieldValueLabel = new JLabel("null"));
	}

	// ArrayList met alle Images
	private ArrayList<JLabel> imageList = new ArrayList<JLabel>();
	{
		imageList.add(pinguin_1 = new JLabel(imageIcon_1 = new ImageIcon("resources/penguin_1.png")));
		imageList.add(pinguin_2 = new JLabel(imageIcon_2 = new ImageIcon("resources/penguin_2.png")));
		imageList.add(pinguin_4 = new JLabel(imageIcon_4 = new ImageIcon("resources/penguin_4.png")));
		imageList.add(pinguin_5 = new JLabel(imageIcon_5 = new ImageIcon("resources/penguin_5.png")));
		imageList.add(ijsbeer_1 = new JLabel(imageIcon_6 = new ImageIcon("resources/ijsbeer_62_62.png")));
	}

	// ArrayList van de Klasse Dobbelsteen
	private ArrayList<Dobbelsteen> dobbelsteenArrayList = new ArrayList<Dobbelsteen>();

	// Enable buttons only when textfields are not empty
	public void insertUpdate(DocumentEvent e) {
		updateButtonEnabledStatus(controlerenButton, reqFieldList);
	}

	public void removeUpdate(DocumentEvent e) {
		updateButtonEnabledStatus(controlerenButton, reqFieldList);
	}

	public void changedUpdate(DocumentEvent e) {
		updateButtonEnabledStatus(controlerenButton, reqFieldList);
	}

	private void updateButtonEnabledStatus(JButton btn, List<JTextField> reqFieldList) {
		boolean enabled = true;
		for (JTextField field : reqFieldList) {
			if (field.getText().length() == 0) {
				enabled = false;
				break;
			}
		}
		btn.setEnabled(enabled);
	}

	private class MyDispatcher implements KeyEventDispatcher {

		public boolean dispatchKeyEvent(KeyEvent e) {

			if (e.getID() == KeyEvent.KEY_PRESSED) {
				if (e.getKeyCode() == KeyEvent.VK_F1) {
					System.out.println("F1 Pressed");
					aantalWakkenField.requestFocus();
				} else if (e.getKeyCode() == KeyEvent.VK_F2) {
					System.out.println("F2 Pressed");
					aantalPinguinsField.requestFocus();
				} else if (e.getKeyCode() == KeyEvent.VK_F3) {
					System.out.println("F3 Pressed");
					aantalIjsberenField.requestFocus();
				} else if (e.getKeyCode() == KeyEvent.VK_F4) {
					System.out.println("F4 Pressed");
					// Volgende hint
				} else if (e.getKeyCode() == KeyEvent.VK_F5) {
					System.out.println("F5 Pressed");
					oplossingButton.doClick();
				} else if (e.getKeyCode() == KeyEvent.VK_F6) {
					System.out.println("F6 Pressed");
					nieuwSpelButton.doClick();
				} else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					System.out.println("Enter Pressed");
					werpButton.doClick();
				} else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					System.out.println("Space Pressed");
					controlerenButton.doClick();
				} else if (e.getKeyCode() == KeyEvent.VK_UP) {
					spinner.requestFocus();
				} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					spinner.requestFocus();
				} else if (e.getKeyCode() == KeyEvent.VK_A) {
					System.out.println("A key Pressed");
					aantalDobbelstenenField.requestFocus();
				}

			}

			return false;
		}

	}

	// Class Werp Handler
	private class WerpHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			// For-loop door arrayList - WerpDobbelsten toevoegen
			for (int i = 0; i < dobbelsteenArrayList.size(); i++) {

				// Doorloop de lijst en voeg de methode WerpDobbelsteen hier aan toe
				dobbelsteenArrayList.get(i).WerpDobbelsteen();

				// Print geworpen getal + iteratie van de huidige arrayList naar console
				System.out.println(" | Dobbelsteen: " + (i + 1) + " " + dobbelsteenArrayList);
			}

			// Elke keer dat de werpen knop word ingedrukt: verhoog totaalGeworpen + 1
			totaalGeworpen++;

			// Set text op het aantalgeworpenField +
			aantalgeworpenField.setText("" + totaalGeworpen);

			if (totaalGeworpen >= 10) {
				aantalgeworpenField.setBorder(BorderFactory.createEmptyBorder(8, 6, 10, 6));
			}
		}
	}

	private class CheckAnswerHandler implements ActionListener {

		// Controleer opgegeven antwoorden
		public void actionPerformed(ActionEvent e) {

			int wakkenInput = Integer.parseInt(aantalWakkenField.getText());
			int pinguinsInput = Integer.parseInt(aantalPinguinsField.getText());
			int ijsberenInput = Integer.parseInt(aantalIjsberenField.getText());

			System.out.println("Wakken invoer: " + wakkenInput);
			System.out.println("Pinguins invoer: " + pinguinsInput);
			System.out.println("Ijsberen invoer: " + ijsberenInput);

			// Creates solution variables
			int pinguinsAnswer = 0;
			int wakkenAnswer = 0;
			int ijsberenAnswer = 0;

			// Get solution
			for (Dobbelsteen dobbelsteen : dobbelsteenArrayList) {
				pinguinsAnswer += dobbelsteen.pinguins;
				wakkenAnswer += dobbelsteen.wakken;
				ijsberenAnswer += dobbelsteen.ijsberen;
			}

			System.out.println("Pinguins correct: " + pinguinsAnswer);
			System.out.println("Wakken correct: " + wakkenAnswer);
			System.out.println("Ijsberen correct: " + ijsberenAnswer);

			// Vergelijking pinguins
			if (pinguinsInput == pinguinsAnswer) {
				aantalPinguinsField.setBackground(Color.GREEN);
				System.out.println("Pinguins aantal is goed");
			} else {
				aantalPinguinsField.setBackground(Color.RED);
				aantalPinguinsField.setForeground(Color.WHITE);
				System.out.println("Pinguins aantal is fout");
			}

			// Vergelijking ijsberen
			if (ijsberenInput == ijsberenAnswer) {
				aantalIjsberenField.setBackground(Color.GREEN);
				System.out.println("Ijsberen aantal is goed");
			} else {
				aantalIjsberenField.setBackground(Color.RED);
				aantalIjsberenField.setForeground(Color.WHITE);
				System.out.println("Ijsberen aantal is fout");
			}

			// Vergelijking wakken
			if (wakkenInput == wakkenAnswer) {
				aantalWakkenField.setBackground(Color.GREEN);
				System.out.println("Wakken aantal is goed");
			} else {
				aantalWakkenField.setBackground(Color.RED);
				aantalWakkenField.setForeground(Color.WHITE);
				System.out.println("Wakken aantal is fout");
			}

			if (pinguinsInput == pinguinsAnswer && ijsberenInput == ijsberenAnswer && wakkenInput == wakkenAnswer) {
				aantalGoedFieldValue++;
				String stringAantalGoedFieldValue = String.valueOf(aantalGoedFieldValue);
				aantalGoedField.setText(stringAantalGoedFieldValue);
				System.out.println(stringAantalGoedFieldValue);
			} else {
				aantalFoutFieldValue++;
				String stringAantalFoutFieldValue = String.valueOf(aantalFoutFieldValue);
				aantalFoutField.setText(stringAantalFoutFieldValue);
				System.out.println(aantalFoutFieldValue);
			}

			// Print updated waardes naar de tekstvelden
			pinguinsFieldAnswer.setText(aantalPinguinsField.getText());
			ijsberenFieldAnswer.setText(aantalIjsberenField.getText());
			wakkenFieldAnswer.setText(aantalWakkenField.getText());

		}

	}

	// Class Reset Game
	private class ResetGame implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			// Leeg de lijst
			dobbelsteenArrayList.clear();

			// Leeg het paneel
			dobbelstenenPaneel.removeAll();

			// For loop met 3 iteraties
			for (int i = 0; i < 3; i++) {
				dobbelsteenArrayList.add(new Dobbelsteen());
			}

			// Voor elke dobbelsteen in arrayList
			for (Dobbelsteen dobbelsteen : dobbelsteenArrayList) {
				dobbelstenenPaneel.add(dobbelsteen);
			}

			// Verwijderd alle getekende objecten van de paintComponent en tekent opnieuw.
			repaint();

			// Revalidate
			revalidate();

			// Zet het aantal van de spinner op 3
			spinner.setValue(Integer.valueOf(3));
			aantalWakkenField.setText("");
			aantalPinguinsField.setText("");
			aantalIjsberenField.setText("");
			pinguinsFieldAnswer.setText("");
			ijsberenFieldAnswer.setText("");
			wakkenFieldAnswer.setText("");
			pinguinsFieldValueAnswer.setText("");
			ijsberenFieldValueAnswer.setText("");
			wakkenFieldValueAnswer.setText("");
			aantalgeworpenField.setText("");
			aantalFoutField.setText("");
			aantalPinguinsField.setBackground(new Color(0XFFDA6B));
			aantalWakkenField.setBackground(new Color(0XFFDA6B));
			aantalIjsberenField.setBackground(new Color(0XFFDA6B));
			// Print message naar console
			System.out.print("Spel Reset. Huidig aantal dobbelstenen in arrayList: " + dobbelsteenArrayList.size());
		}
	}

	// Class voor het verlagen van het aantal dobbelstenen
	private class VerlagenHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			// Get de actuele waarde van de spinner, sla dit op in een variable
			// het datatype van de value van een spinner is altijd een String
			stringHuidigAantal = String.valueOf(spinner.getValue());

			// String to int conversie
			int intHuidigAantal = Integer.parseInt(stringHuidigAantal);

			// Verlaag int huidig aantal op met 1
			intHuidigAantal--;

			// Zet het nieuwe huidige aantal als waarde van de spinner
			spinner.setValue(Integer.valueOf(intHuidigAantal));

		}
	}

	// Class voor het ophogen van het aantal dobbelstenen
	private class OphogenHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {

			// Get de actuele waarde van de spinner, sla dit op in een variable
			// het datatype van de value van een spinner is altijd een String
			stringHuidigAantal = String.valueOf(spinner.getValue());

			// String to int conversie
			int intHuidigAantal = Integer.parseInt(stringHuidigAantal);

			// Hoog int huidig aantal op met 1
			intHuidigAantal++;

			// Zet het nieuwe huidige aantal als waarde van de spinner
			spinner.setValue(Integer.valueOf(intHuidigAantal));

		}
	}

	
	// Class Oplossing Handler
	private class OplossingHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			pinguinsFieldValueAnswer.setText("null");
			ijsberenFieldValueAnswer.setText("null");
			wakkenFieldValueAnswer.setText("null");

			pinguinsFieldValueAnswer.setVisible(true);
			ijsberenFieldValueAnswer.setVisible(true);
			wakkenFieldValueAnswer.setVisible(true);
			
		}
	}

	
	// Paneel Klasse
	Paneel() {

		// Lay-outmanager uitschakelen
		setLayout(null);

		// Achtergrond kleur
		setBackground(new Color(28, 145, 247));

		// Keyboard Focus Manager (Overschrijf de huidige focus manager)
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		manager.addKeyEventDispatcher(new MyDispatcher());

		// Button UIManager Voor opmaak voor alle buttons over de hele applicatie
		UIManager.put("Button.background", Color.BLACK);
		UIManager.put("Button.font", font);
		UIManager.put("Button.foreground", Color.WHITE);
		UIManager.put("Button.select", new Color(116, 114, 214));
		UIManager.put("Button.disabledText", new Color(0XF1F1F1));

		// Required Fields document listeners
		aantalPinguinsField.getDocument().addDocumentListener((DocumentListener) this);
		aantalIjsberenField.getDocument().addDocumentListener((DocumentListener) this);
		aantalWakkenField.getDocument().addDocumentListener((DocumentListener) this);

		// The Vector class implements a growable array of objects
		reqFieldList = new Vector<>();
		{
			reqFieldList.add(aantalPinguinsField);
			reqFieldList.add(aantalIjsberenField);
			reqFieldList.add(aantalWakkenField);
		}

		// Voor elk tekstveld in de arrayList fieldList
		for (JTextField field : fieldList) {
			// Zet horizontal Alignment op center
			field.setHorizontalAlignment(JTextField.CENTER);
			field.setFont(font);
			field.setEditable(false);
			field.setBackground(new Color(0XFFDA6B));
			field.setForeground(Color.BLACK);
		}

		// Voor elk tekstveld in de arrayList reqFieldList
		for (JTextField field : reqFieldList) {
			field.setEditable(true);
		}

		// Voor elk tekstveld in de arrayList reqFieldList
		for (JButton button : buttonList) {
			button.setCursor(new Cursor(Cursor.HAND_CURSOR));
			button.setFont(font);
			button.setFocusPainted(false);
			button.setForeground(Color.WHITE);
		}

		
		/*
		 * Buttons, Jlabels, JTextFields, alignments etc. 
		 * 
		 */

		// Default Button Sizes
		int bedieningsPaneelAlign_Y = 40;
		int buttonSizeX = 150, 		buttonSizeY = 42;
		int fieldSize_XY = 34, 		verhogenVerlagenSizes_XY = 32;
		int labelSizesX = 200, 		labelSizesY = 25;
		
		// Antwoord Controleren Button
		controlerenButton.addActionListener((ActionListener) new CheckAnswerHandler());
		controlerenButton.setEnabled(false);

		// Nieuw spel button
		nieuwSpelButton.addActionListener((ActionListener) new ResetGame());

		// Werp button
		werpButton.addActionListener((ActionListener) new WerpHandler());

		// Verlagen button
		verlagenButton.addActionListener((ActionListener) new VerlagenHandler());

		// Ophogen button
		ophogenButton.addActionListener((ActionListener) new OphogenHandler());

		// Oplossing Button
		oplossingButton.addActionListener((ActionListener) new OplossingHandler());

		oplossingButton.setBounds(125, 320, buttonSizeX, buttonSizeY);


		

		// Nieuw spel button
		int nieuwSpelX = 860, nieuwSpelY = 10;
		int nieuwSpel_sizeX = 150, nieuwSpel_sizeY = 40;

		// Label Correctie
		int labelCorrectie = 2;

		// Input Velden Horizontal Alignment
		int FieldsHorizontalAlignment = 130;

		// Werp button
		int werpButton_X = 62, werpButton_Y = 268;
		int werpButton_SizeX = 150, werpButton_SizeY = 40;

		// Aantal dobbelstenen
		int aantalDobbelstenenField_X = 122, aantalDobbelstenenField_Y = 215;
		int aantalDobbelstenenField_SizeX = 25, aantalDobbelstenenField_SizeY = 25;
		int aantalDobbelstenenLabel_X = 57, aantalDobbelstenenLabel_Y = 100;
		int aantalDobbelstenenLabel_SizeX = 200, aantalDobbelstenenLabel_SizeY = 100;


		// Aantal Wakken
		int aantalWakkenField_X = 180 + FieldsHorizontalAlignment;
		int aantalWakkenField_Y = bedieningsPaneelAlign_Y;
		int aantalWakkenField_SizeX = fieldSize_XY;
		int aantalWakkenField_SizeY = fieldSize_XY;
		int wakkenLabelX = 105 + FieldsHorizontalAlignment;
		int wakkenLabelY = bedieningsPaneelAlign_Y - labelCorrectie;
		int wakkenLabel_SizeX = labelSizesX;
		int wakkenLabel_SizeY = labelSizesY;

		
		
		// Aantal pinguins
		int aantalPinguinsField_X = 330 + FieldsHorizontalAlignment;
		int aantalPinguinsField_Y = bedieningsPaneelAlign_Y;
		int aantalPinguinsField_SizeX = fieldSize_XY;
		int aantalPinguinsField_SizeY = fieldSize_XY;
		int pinguinsLabelX = 250 + FieldsHorizontalAlignment;
		int pinguinsLabelY = bedieningsPaneelAlign_Y - labelCorrectie;
		int pinguinsLabel_SizeX = labelSizesX;
		int pinguinsLabel_SizeY = labelSizesY;

		// Aantal ijsberen
		int aantalIjsberenFieldX = 480 + FieldsHorizontalAlignment;
		int aantalIjsberenFieldY = bedieningsPaneelAlign_Y;
		int aantalIjsberenField_SizeX = fieldSize_XY;
		int aantalIjsberenField_SizeY = fieldSize_XY;
		int aantalIjsberenLabelX = 400 + FieldsHorizontalAlignment;
		int aantalIjsberenLabelY = bedieningsPaneelAlign_Y - labelCorrectie;
		int aantalIjsberenLabel_SizeX = labelSizesX;
		int aantalIjsberenLabel_SizeY = labelSizesY;

		// Antwoord Controleren Button
		int controlerenButtonX = 560 + FieldsHorizontalAlignment;
		int controlerenButtonY = bedieningsPaneelAlign_Y - 8;
		int controlerenButtonSizeX = buttonSizeX;
		int controlerenButtonSizeY = buttonSizeY;

		// Titel Label
		int titelLabelX = 28, titelLabelY = -2;
		int titelLabel_SizeX = 300, titelLabel_SizeY = 60;
		
		ophogenButton.setBounds(62, 180, verhogenVerlagenSizes_XY, verhogenVerlagenSizes_XY);

		verlagenButton.setBounds(180, 180, verhogenVerlagenSizes_XY, verhogenVerlagenSizes_XY);
		
		// Oplossing Velden Links en Rechts
		int OplossingkolomRechts = 250;


		
		// Set Bounds Label & Fields
		aantalGoedField.setBounds(250, 190, fieldSize_XY, fieldSize_XY);

		aantalGoedField.setBounds(250, 180, fieldSize_XY, fieldSize_XY);

		aantalFoutField.setBounds(250, 220, fieldSize_XY, fieldSize_XY);

		aantalGoedLabel.setBounds(10, 190, labelSizesX, labelSizesY);

		aantalFoutLabel.setBounds(10, 227, labelSizesX, labelSizesY);

		
		
		// Hints Label
		int hintsLabel_X = 20, hintsLabel_Y = 120;
		int hintsLabel_SizeX = labelSizesX, hintsLabel_SizeY = labelSizesY;

		// Aantal keren geworpen Field
		int aantalgeworpenField_X = 250;
		int aantalgeworpenField_Y = 260;
		int aantalgeworpenField_size_x = fieldSize_XY;
		int aantalgeworpenField_size_y = fieldSize_XY;

		// Aantal keren geworpen Label
		int aantalGeworpenLabel_X = 10;
		int aantalGeworpenLabel_Y = 200;
		int aantalGeworpenLabel_X_SizeXY = 250;

		// Achtergrond Kleuren
		int backgroundHeaderPaneel = 0xFFFFFF;
		int backgroundControlerenPaneel = 0xFFFFFF;
		int backgroundDobbelstenenPaneel = 0x121212;
		int backgroundBedieningsPaneel = 0xFFFFFF;

		// Pinguin 1
		pinguin_1.setIcon(imageIcon_1);
		pinguin_2.setIcon(imageIcon_2);
		pinguin_4.setIcon(imageIcon_4);
		pinguin_5.setIcon(imageIcon_5);
		ijsbeer_1.setIcon(imageIcon_6);

		// Aantal geworpen JLabel
		aantalGeworpenCounter.setBounds(300, 88, 50, 50);

		// Aantal Wakken
		aantalWakkenField.setBounds(aantalWakkenField_X, aantalWakkenField_Y, aantalWakkenField_SizeX,
				aantalWakkenField_SizeY);
		wakkenLabel.setBounds(wakkenLabelX, wakkenLabelY, wakkenLabel_SizeX, wakkenLabel_SizeY);



		hintsLabel.setBounds(hintsLabel_X, hintsLabel_Y, hintsLabel_SizeX, hintsLabel_SizeY);

		aantalgeworpenField.setBounds(aantalgeworpenField_X, aantalgeworpenField_Y, aantalgeworpenField_size_x,
				aantalgeworpenField_size_y);

		aantalGeworpenLabel.setBounds(aantalGeworpenLabel_X, aantalGeworpenLabel_Y, aantalGeworpenLabel_X_SizeXY, 150);



		// Pinguins Images
		int pinguin_SizeXY = 60;
		pinguin_1.setBounds(540, 8, pinguin_SizeXY, pinguin_SizeXY);
		pinguin_2.setBounds(610, 8, pinguin_SizeXY, pinguin_SizeXY);
		pinguin_4.setBounds(470, 8, pinguin_SizeXY, pinguin_SizeXY);
		pinguin_5.setBounds(400, 12, pinguin_SizeXY, pinguin_SizeXY);

		pinguinsLabel.setBounds(pinguinsLabelX, pinguinsLabelY, pinguinsLabel_SizeX, pinguinsLabel_SizeY);

		aantalPinguinsField.setBounds(aantalPinguinsField_X, aantalPinguinsField_Y, aantalPinguinsField_SizeX,
				aantalPinguinsField_SizeY);

		ijsberenLabel.setBounds(aantalIjsberenLabelX, aantalIjsberenLabelY, aantalIjsberenLabel_SizeX,
				aantalIjsberenLabel_SizeY);

		aantalDobbelstenenField.setBounds(aantalDobbelstenenField_X, aantalDobbelstenenField_Y,
				aantalDobbelstenenField_SizeX, aantalDobbelstenenField_SizeY);

		aantalDobbelstenenLabel.setBounds(aantalDobbelstenenLabel_X, aantalDobbelstenenLabel_Y,
				aantalDobbelstenenLabel_SizeX, aantalDobbelstenenLabel_SizeY);

		aantalIjsberenField.setBounds(aantalIjsberenFieldX, aantalIjsberenFieldY, aantalIjsberenField_SizeX,
				aantalIjsberenField_SizeY);

		titelLabel.setBounds(titelLabelX, titelLabelY, titelLabel_SizeX, titelLabel_SizeY);

		controlerenButton.setBounds(controlerenButtonX, controlerenButtonY, controlerenButtonSizeX,
				controlerenButtonSizeY);

		nieuwSpelButton.setBounds(nieuwSpelX, nieuwSpelY, nieuwSpel_sizeX, nieuwSpel_sizeY);

		werpButton.setBounds(werpButton_X, werpButton_Y, werpButton_SizeX, werpButton_SizeY);

		pinguinsFieldValueAnswer.setBounds(300, 60, fieldSize_XY, fieldSize_XY);
		ijsberenFieldValueAnswer.setBounds(300, 100, fieldSize_XY, fieldSize_XY);
		wakkenFieldValueAnswer.setBounds(300, 140, fieldSize_XY, fieldSize_XY);
		
		pinguinsFieldValueAnswer.setVisible(false);
		ijsberenFieldValueAnswer.setVisible(false);
		wakkenFieldValueAnswer.setVisible(false);

		pinguinsFieldAnswer.setBounds(250, 60, fieldSize_XY, fieldSize_XY);
		ijsberenFieldAnswer.setBounds(250, 100, fieldSize_XY, fieldSize_XY);
		wakkenFieldAnswer.setBounds(250, 140, fieldSize_XY, fieldSize_XY);

		// Label Bounds
		printPinguinsFieldValueLabel.setBounds(10, 60, 250, 50);
		printIjsberenFieldValueLabel.setBounds(10, 100, 250, 50);
		printWakkenFieldValueLabel.setBounds(10, 140, 250, 50);
		printGoedGeradenFieldValueLabel.setBounds(170, 10, 50, 50);
		printFoutGeradenFieldValueLabel.setBounds(220, 10, 50, 50);

		
		
		/*
		 * Jpanelen Variabelen locatie_X, locatie_Y, breedte, wijdte;
		 */

		// Header Paneel
		int headerPaneel_X = 0, headerPaneel_Y = 0;
		int headerPaneel_sizeX = 1050, headerPaneel_sizeY = 60;

		// Dobbelsteen Paneel
		int dobbelsteenPaneel_X = 0, dobbelsteenPaneel_Y = 60;
		int dobbelsteenPaneel_sizeX = 840, dobbelsteenPaneel_sizeY = 300;

		// Bedienings Paneel
		int bedieningsPaneel_X = 800, bedieningsPaneel_Y = 50;
		int bedieningsPaneel_sizeX = 250, bedieningsPaneel_sizeY = 310;

		// Controle Paneel
		int controlerenPaneel_X = 0, controlerenPaneel_Y = 360;
		int controlerenPaneel_sizeX = 1050, controlerenPaneel_sizeY = 110;

		// Grid Paneel
		int gridPaneel_X = 0, gridPaneel_Y = 455;
		int gridPaneel_sizeX = 1050, gridPaneel_sizeY = 410;

		// Hints Paneel
		int hintsPaneel_X = 525, hintsPaneel_Y = 0;
		int hintsPaneel_sizeX = 525, hintsPaneel_sizeY = 410;

		// Oplossing Paneel
		int oplossingPaneel_X = 0, oplossingPaneel_Y = 0;
		int oplossingPaneel_sizeX = 525, oplossingPaneel_sizeY = 410;

		/*
		 * Header Paneel
		 */
		headerPaneel = new JPanel();
		headerPaneel.setBounds(headerPaneel_X, headerPaneel_Y, headerPaneel_sizeX, headerPaneel_sizeY);
		headerPaneel.setBackground(new Color(backgroundHeaderPaneel));
		headerPaneel.setLayout(null);
		headerPaneel.add(titelLabel);
		headerPaneel.add(pinguin_5);
		headerPaneel.add(nieuwSpelButton);

		// Toevoegen aan het hoofdpaneel
		add(headerPaneel);

		
		/*
		 * Dobbelsteen Paneel
		 */
		dobbelstenenPaneel = new JPanel();
		dobbelstenenPaneel.setBounds(dobbelsteenPaneel_X, dobbelsteenPaneel_Y, dobbelsteenPaneel_sizeX,
				dobbelsteenPaneel_sizeY);
		dobbelstenenPaneel.setBackground(new Color(backgroundDobbelstenenPaneel));

		// Toevoegen aan het hoofdpaneel
		add(dobbelstenenPaneel);

		// Dobbelstenen aantal
		aantalDobbelsLabel = new JLabel();
		aantalDobbelsLabel.setBounds(20, 20, 30, 30);

		// Disable verlagen button als default (start is 3 dobbelstenen)
		verlagenButton.setEnabled(false);

		// Spinner Bounds Variables
		int spinnerX = 112, spinnerY = 180;
		int spinner_sizeX = 50, spinner_sizeY = 33;

		// Spinner Value Variables
		int min = 3, max = 12;
		int step = 1, i = 3;

		// Spinner Modal
		SpinnerModel value = new SpinnerNumberModel(i, min, max, step);

		// Spinner Object
		spinner = new JSpinner(value);

		// Huidig Aantal Dobbelstenen - volgens Spinner
		stringHuidigAantal = String.valueOf(spinner.getValue());

		// Spinner Label - Totaal Aantal Dobbelstenen - DEFAULT WAARDE
		aantalDobbelsLabel.setText(stringHuidigAantal);

		// Spinner Bounds
		spinner.setBounds(spinnerX, spinnerY, spinner_sizeX, spinner_sizeY);

		// Spinner Font
		spinner.setFont(new Font("Arial", Font.BOLD, 22));

		// JComponent Spinner
		JComponent comp = spinner.getEditor();

		// Formatted TextField
		JFormattedTextField field = (JFormattedTextField) comp.getComponent(0);

		// Textfield edits
		field.setEditable(true);
		field.setHorizontalAlignment(JTextField.CENTER);
		field.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.BLACK));

		// White border
		spinner.setBorder(BorderFactory.createLineBorder(Color.WHITE));

		// Default Formatter
		DefaultFormatter formatter = (DefaultFormatter) field.getFormatter();
		formatter.setCommitsOnValidEdit(true);

		// int Spinner value
		int spinnerValue = Integer.parseInt(String.valueOf(spinner.getValue()));

		// For-loop door arrayList om dobbelstenen toe te voegen
		for (int j = 0; j < spinnerValue; j++) {
			dobbelsteenArrayList.add(new Dobbelsteen());
		}

		// For-each-loop door arrayList om dobbelstenen toe te voegen aan het
		// dobbelstenen paneel
		for (Dobbelsteen dobbelsteen : dobbelsteenArrayList) {
			dobbelstenenPaneel.add(dobbelsteen);
		}

		// Print aantal dobbelstenen in de arrayList in de console
		System.out.println("Aantal: " + dobbelsteenArrayList.size());

		// Verwijder increment en decrement arrow buttons van de spinner
		for (Component component : spinner.getComponents()) {
			if (component.getName() != null && component.getName().endsWith("Button")) {
				spinner.remove(component);
			}
		}

		// Spinner Change Listener - UPDATED WAARDE
		spinner.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {

				// Nieuwe String met de huidige waarde van de spinner
				huidigAantalDobbelstenen = String.valueOf(spinner.getValue());

				// Set de nieuwe huidige waarde als text op de JLabel
				aantalDobbelsLabel.setText(huidigAantalDobbelstenen);

				// String to int conversie
				int nieuwHuidigAantalDobbelstenen = Integer.parseInt(huidigAantalDobbelstenen);

				// Als het actuele aantal dobbelstenen gelijk is aan 12, disable verhogenButton
				if (nieuwHuidigAantalDobbelstenen == 12) {
					ophogenButton.setEnabled(false);
				} else {
					ophogenButton.setEnabled(true);
				}

				// Als het actuele aantal dobbelstenen kleiner of gelijk is aan 3, Disable
				// verlagenButton
				if (nieuwHuidigAantalDobbelstenen <= 3) {
					verlagenButton.setEnabled(false);
				} else {
					verlagenButton.setEnabled(true);
				}

				// Setter voor nieuwe huidige waarde van de spinner
				SetterAndGetter aantalSetter = new SetterAndGetter(nieuwHuidigAantalDobbelstenen);

				// Switch case met het actuele aantal dobbelstenen
				switch (aantalSetter.getAantal()) {

				// Cases 3 t/m 12 representeren het huidig aantal dobbelstenen
				case 3:

					// Maak de arrayList leeg
					dobbelsteenArrayList.clear();

					// Verwijder alle getekende Componenten van het dobbelsteen paneel
					dobbelstenenPaneel.removeAll();

					// For-loop - Doorloop de arraylist zovaak als het getal van de case
					for (int i = 0; i < 3; i++) {

						// Maak een nieuwe dobbelsteen; voeg dit toe aan de arrayList
						dobbelsteenArrayList.add(new Dobbelsteen());
					}

					// For-each Loop - doorloop de arrayList
					// Voor elke dobbelsteen in de DobbelsteenArraylist
					for (Dobbelsteen dobbelsteen : dobbelsteenArrayList) {

						// Voeg toe aan het paneel
						dobbelstenenPaneel.add(dobbelsteen);
					}

					// Repaint om de hele dobbelsteen te verwijderen van het paneel
					repaint();

					// Print aantal items in de arrayList
					System.out.println("Aantal: " + dobbelsteenArrayList.size());
					break;
				case 4:
					dobbelsteenArrayList.clear();
					dobbelstenenPaneel.removeAll();
					for (int i = 0; i < 4; i++) {
						dobbelsteenArrayList.add(new Dobbelsteen());
					}
					for (Dobbelsteen dobbelsteen : dobbelsteenArrayList) {
						dobbelstenenPaneel.add(dobbelsteen);
					}
					repaint();
					System.out.println("Aantal: " + dobbelsteenArrayList.size());
					break;
				case 5:
					dobbelsteenArrayList.clear();
					dobbelstenenPaneel.removeAll();
					for (int i = 0; i < 5; i++) {
						dobbelsteenArrayList.add(new Dobbelsteen());
					}
					for (Dobbelsteen dobbelsteen : dobbelsteenArrayList) {
						dobbelstenenPaneel.add(dobbelsteen);
					}
					repaint();
					System.out.println("Aantal: " + dobbelsteenArrayList.size());
					break;
				case 6:
					dobbelsteenArrayList.clear();
					dobbelstenenPaneel.removeAll();
					for (int i = 0; i < 6; i++) {
						dobbelsteenArrayList.add(new Dobbelsteen());
					}
					for (Dobbelsteen dobbelsteen : dobbelsteenArrayList) {
						dobbelstenenPaneel.add(dobbelsteen);
					}
					repaint();
					System.out.println("Aantal: " + dobbelsteenArrayList.size());
					break;
				case 7:
					dobbelsteenArrayList.clear();
					dobbelstenenPaneel.removeAll();
					for (int i = 0; i < 7; i++) {
						dobbelsteenArrayList.add(new Dobbelsteen());
					}
					for (Dobbelsteen dobbelsteen : dobbelsteenArrayList) {
						dobbelstenenPaneel.add(dobbelsteen);
					}
					repaint();
					System.out.println("Aantal: " + dobbelsteenArrayList.size());
					break;
				case 8:
					dobbelsteenArrayList.clear();
					dobbelstenenPaneel.removeAll();
					for (int i = 0; i < 8; i++) {
						dobbelsteenArrayList.add(new Dobbelsteen());
					}
					for (Dobbelsteen dobbelsteen : dobbelsteenArrayList) {
						dobbelstenenPaneel.add(dobbelsteen);
					}
					repaint();
					System.out.println("Aantal: " + dobbelsteenArrayList.size());
					break;
				case 9:
					dobbelsteenArrayList.clear();
					dobbelstenenPaneel.removeAll();
					for (int i = 0; i < 9; i++) {
						dobbelsteenArrayList.add(new Dobbelsteen());
					}
					for (Dobbelsteen dobbelsteen : dobbelsteenArrayList) {
						dobbelstenenPaneel.add(dobbelsteen);
					}
					repaint();
					System.out.println("Aantal: " + dobbelsteenArrayList.size());
					break;
				case 10:
					dobbelsteenArrayList.clear();
					dobbelstenenPaneel.removeAll();
					for (int i = 0; i < 10; i++) {
						dobbelsteenArrayList.add(new Dobbelsteen());
					}
					for (Dobbelsteen dobbelsteen : dobbelsteenArrayList) {
						dobbelstenenPaneel.add(dobbelsteen);
					}
					repaint();
					System.out.println("Aantal: " + dobbelsteenArrayList.size());
					break;
				case 11:
					dobbelsteenArrayList.clear();
					dobbelstenenPaneel.removeAll();
					for (int i = 0; i < 11; i++) {
						dobbelsteenArrayList.add(new Dobbelsteen());
					}
					for (Dobbelsteen dobbelsteen : dobbelsteenArrayList) {
						dobbelstenenPaneel.add(dobbelsteen);
					}
					repaint();
					System.out.println("Aantal: " + dobbelsteenArrayList.size());
					break;
				case 12:
					dobbelsteenArrayList.clear();
					dobbelstenenPaneel.removeAll();
					for (int i = 0; i < 12; i++) {
						dobbelsteenArrayList.add(new Dobbelsteen());
					}
					for (Dobbelsteen dobbelsteen : dobbelsteenArrayList) {
						dobbelstenenPaneel.add(dobbelsteen);
					}
					repaint();
					System.out.println("Aantal: " + dobbelsteenArrayList.size());
					break;
				}

			}
		});

		/*
		 * Bedienings Paneel
		 * 
		 **/
		bedieningsPaneel = new JPanel();
		bedieningsPaneel.setLayout(null);
		bedieningsPaneel.setBackground(new Color(backgroundBedieningsPaneel));
		bedieningsPaneel.setBounds(bedieningsPaneel_X, bedieningsPaneel_Y, bedieningsPaneel_sizeX,
				bedieningsPaneel_sizeY);
		bedieningsPaneel.add(ophogenButton);
		bedieningsPaneel.add(verlagenButton);
		bedieningsPaneel.add(aantalDobbelstenenLabel);
		bedieningsPaneel.add(aantalDobbelsLabel);
		bedieningsPaneel.add(werpButton);
		bedieningsPaneel.add(spinner);

		// Toevoegen aan het hoofdpaneel
		add(bedieningsPaneel);

		/*
		 * Controle Paneel
		 */
		controlerenPaneel = new JPanel();
		controlerenPaneel.setLayout(null);
		controlerenPaneel.setBackground(new Color(backgroundControlerenPaneel));
		controlerenPaneel.setBounds(controlerenPaneel_X, controlerenPaneel_Y, controlerenPaneel_sizeX,
				controlerenPaneel_sizeY);
		controlerenPaneel.add(aantalWakkenField);
		controlerenPaneel.add(aantalPinguinsField);
		controlerenPaneel.add(aantalIjsberenField);
		controlerenPaneel.add(wakkenLabel);
		controlerenPaneel.add(pinguinsLabel);
		controlerenPaneel.add(ijsberenLabel);
		controlerenPaneel.add(controlerenButton);
		controlerenPaneel.add(aantalGoedLabel);

		// Toevoegen aan het hoofdpaneel
		add(controlerenPaneel);

		gridPaneel = new JPanel();
		gridPaneel.setLayout(null);
		gridPaneel.setBounds(gridPaneel_X, gridPaneel_Y, gridPaneel_sizeX, gridPaneel_sizeY);
		gridPaneel.setBackground(Color.BLACK);

		hintsPaneel = new JPanel();
		hintsPaneel.setLayout(null);
		hintsPaneel.setBounds(hintsPaneel_X, hintsPaneel_Y, hintsPaneel_sizeX, hintsPaneel_sizeY);
		hintsPaneel.setBackground(new Color(0xF8F8F8));
		hintsPaneel.add(printPinguinsFieldValueLabel);
		hintsPaneel.add(printIjsberenFieldValueLabel);
		hintsPaneel.add(printWakkenFieldValueLabel);
		hintsPaneel.add(jouwAntwoordenLabel);
		hintsPaneel.add(juisteAntwoordenLabel);
		hintsPaneel.add(pinguinsFieldValueAnswer);
		hintsPaneel.add(ijsberenFieldValueAnswer);
		hintsPaneel.add(wakkenFieldValueAnswer);

		oplossingPaneel = new JPanel();
		oplossingPaneel.setLayout(null);
		oplossingPaneel.setBounds(oplossingPaneel_X, oplossingPaneel_Y, oplossingPaneel_sizeX, oplossingPaneel_sizeY);
		oplossingPaneel.setBackground(new Color(0xFFFFFF));

		hintsPaneel.add(aantalFoutLabel);
		hintsPaneel.add(aantalFoutField);
		hintsPaneel.add(aantalGoedLabel);
		hintsPaneel.add(aantalGoedField);
		hintsPaneel.add(totaalGeworpenLabel);
		hintsPaneel.add(aantalgeworpenField);
		hintsPaneel.add(aantalGeworpenLabel);
		hintsPaneel.add(pinguinsFieldAnswer);
		hintsPaneel.add(ijsberenFieldAnswer);
		hintsPaneel.add(wakkenFieldAnswer);
		hintsPaneel.add(oplossingButton);

		// Toevoegen aan sub-paneel
		gridPaneel.add(hintsPaneel);
		gridPaneel.add(oplossingPaneel);

		// Toevoegen aan hoofd-paneel
		add(gridPaneel);

		/**
		 * Button hover models
		 * 
		 */

		// werpButton hover
		werpButton.getModel().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				ButtonModel model = (ButtonModel) e.getSource();
				if (model.isRollover()) {
					werpButton.setForeground(Color.WHITE);
					werpButton.setBackground(new Color(25, 150, 235));
				} else {
					werpButton.setBackground(new Color(25, 150, 250));
					werpButton.setForeground(Color.WHITE);
				}
			}
		});

		// nieuwSpelButton hover
		nieuwSpelButton.getModel().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				ButtonModel model = (ButtonModel) e.getSource();
				if (model.isRollover()) {
					nieuwSpelButton.setForeground(Color.WHITE);
					nieuwSpelButton.setBackground(new Color(25, 150, 235));
				} else {
					nieuwSpelButton.setBackground(new Color(25, 150, 250));
					nieuwSpelButton.setForeground(Color.WHITE);
				}
			}
		});

		// dobbelsteenOphogenButton button hover
		ophogenButton.getModel().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				ButtonModel model = (ButtonModel) e.getSource();
				if (model.isRollover()) {
					ophogenButton.setForeground(Color.WHITE);
					ophogenButton.setBackground(new Color(25, 150, 235));
				} else {
					ophogenButton.setBackground(new Color(25, 150, 250));
					ophogenButton.setForeground(Color.WHITE);
				}
			}
		});

		// dobbelsteenVerlagenButton button hover
		verlagenButton.getModel().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				ButtonModel model = (ButtonModel) e.getSource();
				if (model.isRollover()) {
					verlagenButton.setForeground(Color.WHITE);
					verlagenButton.setBackground(new Color(25, 150, 235));
				} else {
					verlagenButton.setBackground(new Color(25, 150, 250));
					verlagenButton.setForeground(Color.WHITE);
				}
			}
		});

		// dobbelsteenVerlagenButton button hover
		oplossingButton.getModel().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				ButtonModel model = (ButtonModel) e.getSource();
				if (model.isRollover()) {
					oplossingButton.setForeground(Color.WHITE);
					oplossingButton.setBackground(new Color(25, 150, 235));
				} else {
					oplossingButton.setBackground(new Color(25, 150, 250));
					oplossingButton.setForeground(Color.WHITE);
				}
			}
		});

		// controlerenButton button hover
		controlerenButton.getModel().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				ButtonModel model = (ButtonModel) e.getSource();
				if (model.isRollover()) {
					controlerenButton.setForeground(Color.WHITE);
					controlerenButton.setBackground(new Color(25, 150, 235));
				} else {
					controlerenButton.setBackground(new Color(25, 150, 250));
					controlerenButton.setForeground(Color.WHITE);
				}
			}
		});

	}

}
