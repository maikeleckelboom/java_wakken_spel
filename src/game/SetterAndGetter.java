package game;

public class SetterAndGetter {
	
    // Private member variable, so that nobody outside this class can access it directly
	public int aantal;
	
	// Constructor
    public SetterAndGetter(int aantal) {
		this.aantal = aantal;
	}


    // Setter method
    public void setAantal(int newAantal) {
        this.aantal = newAantal;
        // Call some other method here, to notify that the number has changed
        
    }
    
	// Getter method
    public int getAantal() {
        return this.aantal;
    }
    
}
