package game;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JTextField;

public class MyFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public class MyDispatcher implements KeyEventDispatcher {
		@Override
		public boolean dispatchKeyEvent(KeyEvent e) {
			if (e.getID() == KeyEvent.KEY_PRESSED) {
				if (e.getKeyCode() == KeyEvent.VK_F1) {
					System.out.println("F1 Pressed");
//					dispose();
					// Actie hier toevoegen - Toets geklikt
					
				}
			} else if (e.getID() == KeyEvent.KEY_RELEASED) {
				if (e.getKeyCode() == KeyEvent.VK_F1) {
					System.out.println("F1 Released");
//					dispose();
					// Actie hier toevoegen - Toets los gelaten
					
					
				}
			} else if (e.getID() == KeyEvent.KEY_TYPED) {
				if (e.getKeyCode() == KeyEvent.VK_F1) {
					System.out.println("F1 Typed");
//					dispose();
					// Actie hier toevoegen - Getypt met de toets
					
					
				}
			}
			return false;
		}
	
	}

	public MyFrame() {
		add(new JTextField());
		KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		manager.addKeyEventDispatcher(new MyDispatcher());
	}

	public static void main(String[] args) {
		MyFrame f = new MyFrame();
		f.pack();
		f.setVisible(true);
	}

}